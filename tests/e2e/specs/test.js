// https://docs.cypress.io/api/introduction/api.html

describe('Test sending form', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  
  it('Display error on submit', () => {
    cy.get('.ns.labeled.button')
      .click()
    cy.get('div.invalid-feedback').should('contain', 'Le champ Prénom est obligatoire.')
    cy.get('div.invalid-feedback').should('contain', 'Le champ Nom est obligatoire.')
    cy.get('div.invalid-feedback').should('contain', 'Le champ Code postal est obligatoire.')
    cy.get('div.invalid-feedback').should('contain', 'Le champ Ville est obligatoire.')
    cy.get('div.invalid-feedback').should('contain', 'Le champ Raison social est obligatoire.')
    cy.get('div.invalid-feedback').should('contain', 'Le champ Téléphone est obligatoire.')
  })

  it('Send correctly data', () => {

    cy.server()

    cy.route('POST', '/demand').as('postDemand')

    cy.get('#civility label[for=civility__BV_option_1_]')
    .click()

    cy.get('input[name=firstname]')
    .type('Tony')

    cy.get('input[name=lastname]')
    .type('Stark')

    cy.get('input[name=pc]')
    .type('75000')

    cy.get('input[name=city]')
    .type('paris')

    cy.get('input[name=reason]')
    .type('sas')

    cy.get('input[name=phone]')
    .type('0123456789')

    cy.get('#choice label[for=__BVID__32]')
    .click()

    cy.get('.ns.labeled.button')
      .click()

    cy.wait('@postDemand')
  })
})
