# CEV exercice

## But de l'exercice

1. L’intégration d’un email selon la maquette PSD (emailing.psd)
2. L’intégration d’un formulaire selon la maquette PSD (maquette.psd)
3. De plus, il faudrait réaliser un test Vue.JS, une fois le formulaire intégré, il faudrait :
  * Simuler son envoi dans la même page (POST)
  * Y récupérer les données transmises en Vue.JS
  * Les sécuriser pour une éventuelle mise en BDD
  * Les afficher sur la même page où vous le souhaitez

>>>
Le template html d'emailing ce trouve dans le dossier **src/emailing**

Les maquettes ce trouvent dans le dossier **src/maquettes**

Pour tester le formulaire, suivre les indications ci-dessou
>>>

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```