import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import VeeValidate from 'vee-validate'
import validationMessagesFr from 'vee-validate/dist/locale/fr'

Vue.config.productionTip = false

Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  errorBagName: 'veeErrors',
  events: 'change|blur',
  locale: 'fr',
  dictionary: {
    fr: validationMessagesFr
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
